const net = require('net');

const clipMonit = require('clipboard-monitor');
var monitor = clipMonit(500);

const client = net.createConnection({ port: 8888 }, () => {
  // 'connect' 监听器
  console.log('已连接到服务器');
});

monitor.on('copy', function (data) {
  //do something with the data
  console.log(data);
  client.write(data);
});

function OnData(data){
  console.log(data.toString());
}

client.on('data', OnData);

client.on('error', err =>{
    console.log(err.message);
});



client.on('end', () => {
  console.log('已从服务器断开');
});