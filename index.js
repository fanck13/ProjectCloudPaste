'use strict';
const events = require('events');
const net = require('net');
const channel = new events.EventEmitter();

/*const clipMonit = require('clipboard-monitor');
var monitor = clipMonit(500);
monitor.on('copy', function (data) {
    //do something with the data
    console.log(data);
});*/

channel.clients = {};
channel.subscriptions = {};

function JoinChannel(id, client){
  channel.clients[id] = client;
  channel.subscriptions[id] = (senderId, message) => {
    if (id != senderId) {
      channel.clients[id].write(message);
    }
  };
  channel.on('broadcast', channel.subscriptions[id]);
}

channel.on('join', JoinChannel);

const server = net.createServer(client => {
  const id = `${client.remoteAddress}:${client.remotePort}`;
  channel.emit('join', id, client);
  client.on('data', data => {
    data = data.toString();
    channel.emit('broadcast', id, data);
  });
});


server.listen(8888);


//Stop the monitoring the clipboard
//monitor.end();